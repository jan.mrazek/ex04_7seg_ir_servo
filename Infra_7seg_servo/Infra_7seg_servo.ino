#include <Servo.h>


//Grove 4-digit display by Seeed Studio 1.0.0
#include <TM1637.h>

//IRremote by shirriff 2.2.3
#include <boarddefs.h>
#include <IRremote.h>
#include <ir_Lego_PF_BitStreamEncoder.h>
#include <IRremoteInt.h>


#define CLK (4)
#define DIO (5)
#define SERVO (6)
#define IRDA (A0)

int angle;

TM1637 lcd(CLK, DIO);

IRrecv ir(IRDA);
decode_results result;
Servo servo;

long codes[20]=
{
0xFD708F, 0xFD08F7, 0xFD8877, 0xFD48B7, 0xFD28D7, //0, 1, 2, 3, 4
0xFDA857, 0xFD6897, 0xFD18E7, 0xFD9867, 0xFD58A7, //5, 6, 7, 8, 9, 
0xFD20DF, 0xFD30CF, 0xFDA05F, 0xFDB04F, 0xFD10EF, // +, -, UP, DWN, L 
0xFD50AF, 0xFD906F, 0xFD00FF, 0xFD40BF, 0xFD609F //R, OK, ON/OFF, MENU, BACK
};

void setup() {
  lcd.init();
  lcd.set(BRIGHT_TYPICAL);

  pinMode(SERVO, OUTPUT);

  servo.attach(SERVO);

  ir.enableIRIn();

  angle = 90;
}

void loop() {
  if(ir.decode(&result)) {
    int i=0;
    while(i<=9) {
      if(result.value == codes[i]) {
        ir.resume();
        lcd.clearDisplay();
        lcd.display(3,i);        
        angle = (i+1)*18;
        servo.write(angle);
        delay(20);
      }
      i++;
    }
    if(result.value == 0xFD00FF) {
      lcd.clearDisplay();
      digitalWrite(SERVO, LOW);
    }
    if(result.value < 0xFE0000) {
    //Serial.println(result.value, HEX);
    }
    ir.resume();
  }
  //servo.write(angle);
}
